
package video;

import java.util.ArrayList;


public class CapaLogica {

    ArrayList<Empleado> empleados = new ArrayList<>();
    ArrayList<Cliente> clientes = new ArrayList<>();
    ArrayList<Peliculas> peliculas = new ArrayList<>();

    public void registrarEmpleado(Empleado obj) {
        //agregar el elemento al array list

        empleados.add(obj);
    }

    public String[] getEmpleados() {
        String[] data = new String[empleados.size()];
        int posicion = 0;
        //recorrer araylist con for each

        for (Empleado dato : empleados) {
            String info = dato.getNombre() + " " + dato.getApellidos() + " "
                    + dato.getIdentificacion() + " " + dato.getCorreo() + " "
                    + dato.getClave();

            data[posicion] = info;
            posicion++;

        }
        return data;
    }

    public void registrarCliente(Cliente obj) {
        //agregar el elemento al array list

        clientes.add(obj);
    }

    public String[] getClientes() {
        String[] data = new String[clientes.size()];
        int posicion = 0;
        //recorrer araylist con for each

        for (Cliente dato : clientes) {
            String info = dato.getNombre() + " " + dato.getApellidos() + " "
                    + dato.getIdentificacion() + " " + dato.getCorreo() + " "
                    + dato.getClave() + " " + dato.getClave() + " " + dato.getDireccion();

            data[posicion] = info;
            posicion++;

        }
        return data;
    }

    public void registrarPeliculas(Peliculas obj) {
        peliculas.add(obj);
    }

    public String[] getPeliculas() {
        String[] data = new String[peliculas.size()];
        int posicion = 0;

        for (Peliculas dato : peliculas) {
            String info = dato.getCodigo() + "" + dato.getTitulo() + "" + dato.getAnno() + "" + dato.getArgumento() + "" + dato.getActores() + "" + dato.getDirectores();

            data[posicion] = info;
            posicion++;
        }
        return data;
    }
}

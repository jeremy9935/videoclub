package video;

import java.io.*;

public class Main {

    public static BufferedReader leer = new BufferedReader(
            new InputStreamReader(System.in));
    public static PrintStream imprimir = System.out;
    static Controller gestor = new Controller();

    public static void main(String[] args) throws IOException {

        crearMenu();
    }

    public static void crearMenu() throws IOException {
        boolean noSalir;
        int opcion;
        do {
            mostrarMenu();
            opcion = leerOpcion();
            noSalir = ejecutarAccion(opcion);

        } while (noSalir);

    }

    public static void mostrarMenu() throws IOException {

        imprimir.println("1.    Registrar Empleado");
        imprimir.println("2.    Listar Empleados");
        imprimir.println("3.    Registrar Cliente");
        imprimir.println("4.    Listar Clientes");
        imprimir.println("5.    Registrar Pelicula");
        imprimir.println("6.    Listar Peliculas");
        imprimir.println("7.    Salir");

        imprimir.println("");
    }// FIN MOSTRAR  MENÚ

    public static int leerOpcion() throws IOException {
        int opcion;

        imprimir.println("Seleccione su opción que desee:\n");
        opcion = Integer.parseInt(leer.readLine());
        imprimir.println();
        return opcion;

    }// FIN LEER OPCION

    public static boolean ejecutarAccion(int popcion) throws IOException {
        boolean noSalir = true;
        boolean define;
        switch (popcion) {

            case 1:
                registrarEmpleado();

                break;
            case 2:
                listarEmpleado();

                break;
            case 3:
                registrarCliente();

                break;
            case 4:
                listarCliente();

                break;
            case 5:
                registrarPelicula();
                break;
            case 6:
                listarPeliculas();
                break;
            case 7:
                imprimir.println("Adiós");
                noSalir = false;
                break;

            default:
                imprimir.println("¡OPCIÓN INVÁLIDA!" + "\n"
                        + "Por favor inténtelo nuevamente");
                imprimir.println();
                break;
        }

        return noSalir;

    }// Fin de 

    public static void registrarEmpleado() throws IOException {
        String nombre;
        String apellidos;
        String identificacion;
        String correo;
        String clave;

        imprimir.println("Digite el nombre del empleado");
        nombre = leer.readLine();
        imprimir.println("Digite los apellidos del empleado");
        apellidos = leer.readLine();
        imprimir.println("Digite la identificación del empleado");
        identificacion = leer.readLine();
        imprimir.println("Digite el correo del empleado");
        correo = leer.readLine();
        imprimir.println("Digite la clave del empleado");
        clave = leer.readLine();

        gestor.procesarEmpleado(nombre, apellidos, correo, identificacion, clave);

    }

    public static void listarEmpleado() throws IOException {
        String[] empleados = gestor.listaEmpleados();
        
        Empleado p = new Empleado();
        imprimir.println(p);
        
        for (String dato : empleados) {
            imprimir.println(dato);

        }

    }

    public static void registrarCliente() throws IOException {
        String nombre;
        String apellidos;
        String identificacion;
        String correo;
        String clave;
        String codigo;
        String direccion;

        imprimir.println("Digite el nombre del cliente");
        nombre = leer.readLine();
        imprimir.println("Digite los apellidos del cliente");
        apellidos = leer.readLine();
        imprimir.println("Digite la identificación del cliente");
        identificacion = leer.readLine();
        imprimir.println("Digite el correo del cliente");
        correo = leer.readLine();
        imprimir.println("Digite la clave del cliente");
        clave = leer.readLine();
        imprimir.println("Digite el código del cliente");
        codigo = leer.readLine();
        imprimir.println("Digite la dirección del cliente");
        direccion = leer.readLine();

        gestor.procesarCliente(nombre, apellidos, identificacion,
                correo, clave, codigo, direccion);

    }

    public static void listarCliente() throws IOException {
        String[] clientes = gestor.listaClientes();
        
        Cliente c = new Cliente();
        imprimir.println(c);
        
        for (String dato : clientes) {
            imprimir.println(dato);
        }

    }

    public static void registrarPelicula() throws IOException {
        String codigo;
        String titulo;
        String anno;
        String argumento;
        String actores;
        String director;
        boolean estreno;

        imprimir.println("Digite el codigo de la pelicula");
        codigo = leer.readLine();
        imprimir.println("Digite el titulo de la pelicula");
        titulo = leer.readLine();
        imprimir.println("Digite el año de la pelicula");
        anno = leer.readLine();
        imprimir.println("Digite el argumento de la pelicula");
        argumento = leer.readLine();
        imprimir.println("Digite los actores de la pelicula");
        actores = leer.readLine();
        imprimir.println("Digite el o los directores");
        director = leer.readLine();

    }

    public static void listarPeliculas() throws IOException {
        String[] peliculas = gestor.listarPeliculas();
        int i = 0;
        
        if (i==0){
        Peliculas pp =  new Peliculas();
        imprimir.println(pp);
        i++;
        }
        
        for (String dato : peliculas) {
            imprimir.println(dato);
        }

    }
}

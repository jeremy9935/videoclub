package video;


/**
 *
 * @author pc
 */
public class Cliente {

    private String nombre;
    private String apellidos;
    private String identificacion;
    private String correo;
    private String clave;
    private String codigo;
    private String direccion;

    public Cliente() {
        this.nombre = "";
        this.apellidos = "";
        this.identificacion = "";
        this.correo = "";
        this.clave = "";
        this.codigo = "";
        this.direccion = "";
    }

    
    

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public String getCorreo() {
        return correo;
    }

    public String getClave() {
        return clave;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getDireccion() {
        return direccion;
    }

    @Override
    public String toString() {
        return "Cliente{" + "nombre=" + nombre + ", apellidos=" + apellidos + ", identificacion=" + identificacion + ", correo=" + correo + ", clave=" + clave + ", codigo=" + codigo + ", direccion=" + direccion + '}';
    }
    
    

}

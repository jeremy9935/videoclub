package video;

public class Peliculas {

    private String codigo;
    private String titulo;
    private String anno;
    private String argumento;
    private String actores;
    private String directores;
    private boolean estreno;

    public Peliculas() {
        this.codigo = "";
        this.titulo = "";
        this.anno = "";
        this.argumento = "";
        this.actores = "";
        this.directores = "";
    }

    public String getCodigo() {
        return codigo;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getAnno() {
        return anno;
    }

    public String getArgumento() {
        return argumento;
    }

    public String getActores() {
        return actores;
    }

    public String getDirectores() {
        return directores;
    }

    public boolean isEstreno() {
        return estreno;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setAnno(String anno) {
        this.anno = anno;
    }

    public void setArgumento(String argumento) {
        this.argumento = argumento;
    }

    public void setActores(String actores) {
        this.actores = actores;
    }

    public void setDirectores(String directores) {
        this.directores = directores;
    }

    public void setEstreno(boolean estreno) {
        this.estreno = estreno;
    }

    @Override
    public String toString() {
        return "Peliculas{" + "codigo=" + codigo + ", titulo=" + titulo + ", anno=" + anno + ", argumento=" + argumento + ", actores=" + actores + ", directores=" + directores + ", estreno=" + estreno + '}';
    }
    
    
    
}

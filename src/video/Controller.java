package video;

import java.time.LocalDate;

public class Controller {

    CapaLogica logica = new CapaLogica();

    public Controller() {
    }

    public void procesarEmpleado(String nombre, String apellidos,
            String correo, String identificacion, String clave) {

        Empleado tmpEmpleado;
        tmpEmpleado = new Empleado();

        tmpEmpleado.setNombre(nombre);
        tmpEmpleado.setApellido(apellidos);
        tmpEmpleado.setCorreo(correo);
        tmpEmpleado.setIdentificacion(identificacion);
        tmpEmpleado.setClave(clave);
        tmpEmpleado.setFechaNacimiento(LocalDate.now());

        logica.registrarEmpleado(tmpEmpleado);

    }

    public String[] listaEmpleados() {
        return logica.getEmpleados();
    }

    public void procesarCliente(String nombre, String apellidos,
            String identificacion, String correo, String clave, String codigo,
            String direccion) {

        Cliente tmpCliente;
        tmpCliente = new Cliente();

        tmpCliente.setNombre(nombre);
        tmpCliente.setApellidos(apellidos);
        tmpCliente.setCorreo(correo);
        tmpCliente.setIdentificacion(identificacion);
        tmpCliente.setClave(clave);
        tmpCliente.setCodigo(codigo);
        tmpCliente.setDireccion(direccion);
        logica.registrarCliente(tmpCliente);
    }

    public String[] listaClientes() {
        return logica.getClientes();
    }

    public void procesarPelicula(String codigo, String titulo,
            String anno, String argumento, String actores, String directtores) {

        Peliculas tmpPeliculas;
        tmpPeliculas = new Peliculas();

        tmpPeliculas.setCodigo(codigo);
        tmpPeliculas.setTitulo(titulo);
        tmpPeliculas.setAnno(anno);
        tmpPeliculas.setArgumento(argumento);
        tmpPeliculas.setActores(actores);
        tmpPeliculas.setDirectores(directtores);
        tmpPeliculas.setEstreno(false);

        logica.registrarPeliculas(tmpPeliculas);
    }

    public String[] listarPeliculas() {
        return logica.getPeliculas();

    }
}

package video;

import java.time.LocalDate;

public class Empleado {

    private String identificacion;
    private String nombre;
    private String apellidos;
    private String correo;
    private String clave;
    private int edad;
    private LocalDate fechaNacimiento;

    public Empleado() {
        this.identificacion = "";
        this.nombre = "";
        this.apellidos = "";
        this.correo = "";
        this.clave = "";
        this.edad = edad;
    }

    public int getEdad() {
        return edad;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fecha) {
        this.fechaNacimiento = fecha;
        /*Hacer la logica parab calcular la edad al día de hoy
que es La fecha de hoy menos La fecha de nacimiento.
edad=......;*/
    }

    public String getIdentificacion() {
        return this.identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombre() {

        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {

        return this.apellidos;
    }

    public void setApellido(String apellido) {
        this.apellidos = apellido;
    }

    public String getCorreo() {
        return this.correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getClave() {
        return this.clave;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    @Override
    public String toString() {
        return "Empleado{" + "identificacion=" + identificacion + ", nombre=" +
                nombre + ", apellidos=" + apellidos + ", correo=" + correo +
                ", clave=" + clave + ", edad=" + edad + '}';
    }

}
